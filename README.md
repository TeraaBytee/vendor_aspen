# G-cam Aspen mod app for Begonia

Notes:
* In order not to have conflicts with other mods
* Use attached .xml config for best result

For build, add to *.mk
$(call inherit-product, vendor/aspen/config.mk)

you need to cherry-pick this commit for buffer fix:
https://github.com/TeraaBytee/vendor_redmi_begonia/commit/eff0d7c5b4919da2dc0dc24b0c8e6346a6aeeda2
